ScrollReveal().reveal('.scrollreveral', {
  duration: 800, // アニメーションの完了にかかる時間
  viewFactor: 0.2, // 0~1,どれくらい見えたら実行するか
  origin: 'bottom',
  distance: '50px'
});
ScrollReveal().reveal('.scrollreveral2', {
  duration: 800, // アニメーションの完了にかかる時間
  viewFactor: 0.2, // 0~1,どれくらい見えたら実行するか
  origin: 'bottom',
  distance: '50px',
  delay:50
});
ScrollReveal().reveal('.scrollreveral3', {
  duration: 800, // アニメーションの完了にかかる時間
  viewFactor: 0.2, // 0~1,どれくらい見えたら実行するか
  origin: 'bottom',
  distance: '50px',
  delay:100
});
ScrollReveal().reveal('.scrollreveral4', {
  duration: 800, // アニメーションの完了にかかる時間
  viewFactor: 0.2, // 0~1,どれくらい見えたら実行するか
  origin: 'bottom',
  distance: '50px',
  delay:150
});
