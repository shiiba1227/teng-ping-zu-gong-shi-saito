<!DOCTYPE html>
<?php
  $path = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
?>
<html lang="ja">
<head>
  <title>【公式】株式会社藤平組 | 地域に密着した和歌山県の総合建設会社です。</title>
  <meta content="株式会社藤平組は地域に密着した和歌山県の総合建設会社です。" name="description">
  <?php include('head.php'); ?>
  <link href="./css/joblist.css" rel="stylesheet" type="text/css">
</head>

<body id="home" class="home blog drawer drawer--right">
  <div id="joblist">
<?php include('header.php'); ?>

<main>
  <section id="title">
    <h1><span>JOB LIST</span>求人一覧</h1>
  </section>

  <div class="wrapper">
  <div id="sekokan">
  <div class="joblist container flex">
    <div class="job_box">
      <a href="./sekokan.php" alt="求人情報詳細">
      <div class="job_title flex">
        <div class="job_img"><img src="./image/joblist/sekokan.png"><span>施工管理</span></div>
        <div class="job_txt">
          <span>［和歌山県岩出市］働く人ファーストを掲げ、地元地域に高度なインフラ技術で貢献します！施工管理（岩出市中黒）。幅広い知識と経験を蓄積していただきます。</span>
          株式会社 藤平組
        </div>
      </div>
      </a>
    </div>
      <div class="conditions">
        <table>
          <tr><td>給与</td><th>月給280,000円〜400,000円</th></tr>
          <tr><td>勤務地</td><th>和歌山県岩出市中黒１６４番地の４</th></tr>
          <tr><td>勤務時間</td><th>フルタイム</th></tr>
        </table>
      </div>
    <div class="btn flex">
      <a href="./sekokan.php"><div class="more_btn">詳しく見る</div></a>
      <a href="entry.php"><div class="form_btn">応募する</div></a>
      <a href=""><div class="line_btn">LINE応募</div></a>
    </div>
  </div>
  </div>
  <div id="assist">
  <div class="joblist container flex">
    <div class="job_box">
      <a href="./assist.php" alt="求人情報詳細">
      <div class="job_title flex">
        <div class="job_img"><img src="./image/joblist/assist.png"><span>施工管理補助</span></div>
        <div class="job_txt">
          <span>［和歌山県岩出市］働く人ファーストを掲げ、地元地域に高度なインフラ技術で貢献します！施工管理（岩出市中黒）。幅広い知識と経験を蓄積していただきます。</span>
          株式会社 藤平組
        </div>
      </div>
      </a>
    </div>
      <div class="conditions">
        <table>
          <tr><td>給与</td><th>月給 243,000円〜305000円</th></tr>
          <tr><td>勤務地</td><th>和歌山県岩出市中黒１６４番地の４</th></tr>
          <tr><td>勤務時間</td><th>フルタイム</th></tr>
        </table>
      </div>
    <div class="btn flex">
      <a href="./assist.php"><div class="more_btn">詳しく見る</div></a>
      <a href="entry.php"><div class="form_btn">応募する</div></a>
      <a href=""><div class="line_btn">LINE応募</div></a>
    </div>
  </div>
  </div>
  <div id="worker">
  <div class="joblist container flex">
    <div class="job_box">
      <a href="./worker.php" alt="求人情報詳細">
      <div class="job_title flex">
        <div class="job_img"><img src="./image/joblist/worker.png"><span>現場スタッフ</span></div>
        <div class="job_txt">
          <span>［和歌山県岩出市］働く人ファーストを掲げ、地元地域に高度なインフラ技術で貢献します！施工管理（岩出市中黒）。幅広い知識と経験を蓄積していただきます。</span>
          株式会社 藤平組
        </div>
      </div>
      </a>
    </div>
      <div class="conditions">
        <table>
          <tr><td>給与</td><th>月給 249,700円〜317,800円</th></tr>
          <tr><td>勤務地</td><th>和歌山県岩出市中黒１６４番地の４</th></tr>
          <tr><td>勤務時間</td><th>フルタイム</th></tr>
        </table>
      </div>
    <div class="btn flex">
      <a href="./worker.php"><div class="more_btn">詳しく見る</div></a>
      <a href="entry.php"><div class="form_btn">応募する</div></a>
      <a href=""><div class="line_btn">LINE応募</div></a>
    </div>
  </div>
  </div>
</div>


</div>
</main>
</div>

<?php include('footer.php'); ?>

</body>

</html>
