<!DOCTYPE html>
<?php
  $path = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
?>
<html lang="ja">
<head>
  <title>【公式】株式会社藤平組 | 地域に密着した和歌山県の総合建設会社です。</title>
  <meta content="株式会社藤平組は地域に密着した和歌山県の総合建設会社です。" name="description">
  <?php include('head.php'); ?>
  <link href="./css/line.css" rel="stylesheet" type="text/css">
</head>

<body id="home" class="home blog drawer drawer--right">
<div id="line">
<?php include('header.php'); ?>
<main>


<section id="title">
  <h1><span>LINE ENTRY GUIDE</span>LINE応募使い方ガイド</h1>
</section>

<article class="step1">
<div class="wrapper">
<h2><span>STEP1</span></h2>
<p>LINE応募ボタンを押すと、<br class="pc">採用担当の友達追加ページが表示されるので、「追加」を押してください。</p>
<img src="./image/line/line1.png">
</div>
</article>

<article class="step2">
<div class="wrapper">
<h2><span>STEP2</span></h2>
<p>友達追加ボタンを押すと、採用担当から自動返信メッセージが届きます。<br class="pc">氏名・電話番号・生年月日・運転免許情報を返信してください。</p>
<img src="./image/line/line2.png">
</div>
</article>

<article class="step3">
<div class="wrapper">
<h2><span>STEP3</span>完了！</h2>
<p>情報を送信内容後、採用担当者から２営業日以内に<br class="pc">面接のご案内または、電話にてご連絡する旨のメッセージが届きます。</p>
<img src="./image/line/line3.png">
</div>
</article>

<div class="btn flex">
  <a href="https://lin.ee/tc2pkAt" target="_blank"><div class="line_btn">LINE応募</div></a>
  <a href="entry.php"><div class="form_btn">応募する</div></a>
</div>

</div>
</main>
</div>

<?php include('footer.php'); ?>

</body>
</html>
