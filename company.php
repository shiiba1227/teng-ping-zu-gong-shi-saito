<!DOCTYPE html>
<?php
  $path = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
?>
<html lang="ja">
<head>
  <title>【公式】株式会社藤平組 | 地域に密着した和歌山県の総合建設会社です。</title>
  <meta content="株式会社藤平組は地域に密着した和歌山県の総合建設会社です。" name="description">
  <?php include('head.php'); ?>
  <link href="./css/company.css" rel="stylesheet" type="text/css">
</head>

<body id="home" class="home blog drawer drawer--right">
<div id="company">
<?php include('header.php'); ?>

<main>

<section id="title">
  <h1><span>COMPANY</span>企業情報</h1>
</section>

<section id="outline">
  <div class="wrapper">
  <h2><span>OUTLINE</span>会社概要</h2>
  <table>
    <tr><th>社名</th><td>株式会社 藤平組</td></tr>
    <tr><th>代表者</th><td>藤平 良光</td></tr>
    <tr>
      <th>営業所所在地</th>
      <td>〒649-6248 和歌山県岩出市中黒164-4<br>TEL:0736-62-2041</td>
    </tr>
    <tr><th>Email</th><td>fujihira@beach.ocn.ne.jp</td></tr>
    <tr><th>資本金</th><td>3,500万円</td></tr>
    <tr><th>設立</th><td>1955年</td></tr>
    <tr><th>従業員数</th><td>24名(R2年8月現在)</td></tr>
    <tr>
      <th>所属団体</th>
      <td>
        社団法人 和歌山県建設業協会 / 那賀建設業協会 / 岩出市建設業協会 / 社団法人 和歌山県産業資源循環協会 / 社団法人 和歌山県建築士事務所協会 / 岩出市商工会
      </td>
    </tr>
    <tr>
      <th>建設業許可番号</th>
      <td>和歌山県知事　許可(特定-29）第702号<br>
      建設業の種類<br>
      土木工事業 / 建築工事業 / 大工工事業 / とび・土工工事業 / 石工事業 / 屋根 / 工事業 / タイル・れんが・ブロック工事業 /鋼構造物工事業 / ほ装工事業 / しゅん
      せつ工事業 / 内装仕上工事業 / 熱絶縁工事業 / 水道施設工事業/解体工事業 /
      和歌山県知事　許可(一般-29）第702号<br>
      建設業の種類<br>
      管工事業 / 造園工事業 / 鉄筋工事業</td>
    </tr>
    <tr>
      <th>一級建築士事務所</th><td>和歌山県知事 登録 (ﾄ)第972-3号</td>
    </tr>
    <tr>
      <th>測量事務所</th><td>国土交通省近畿地方整備局 登録 第(3)-32383号</td>
    </tr>
    <tr>
      <th>産業廃棄物収集運搬許可番号</th><td>和歌山県知事 許可 第03001034570号</td>
    </tr>
    <tr>
      <th>資格者数</th>
      <td>一級建築士 2名、二級建築士 2名、一級土木施工管理技士 13名、二級土木施工管、理技士 2名、一級建築施工管理技士 4名、二級建築施工管理技士 3名、一級建設機
      、械施工技士 1名、一級管工事施工管理技士 1名、二級管工事施工管理技士 2名、一級舗装施工管理技士 2名、二級舗装施工管理技士 1名、二級造園管理技士 1名、測量士 5名、測量士補 1名、ｺﾝｸﾘｰﾄ技士 1名、技術士補 1名、特定建築物調査士 3名、
      、推進工事士 7名、給水装置工事主任技術者 1名、下水道排水設備工事責任技術者 6名、二級建設業経理士 1名※上記資格は重複して記載</td>
    </tr>
    <tr>
      <th>主要取引先</th>
      <td>国・中央官庁、和歌山県、岩出市、民間</td>
    </tr>
    <tr>
      <th>取引銀行</th><td>南都銀行岩出支店、紀陽銀行岩出支店</td>
    </tr>
  </table>
  </div>

  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3297.2508548774936!2d135.28551095093871!3d34.267654813699195!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000b54d00728001%3A0xffa782d234427469!2z44CSNjQ5LTYyNDgg5ZKM5q2M5bGx55yM5bKp5Ye65biC5Lit6buS77yR77yW77yU4oiS77yU!5e0!3m2!1sja!2sjp!4v1609330852012!5m2!1sja!2sjp" width="1920" height="500" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

</section>

<section id="top_message">
  <h2><span>TOP MESSAGE</span>ご挨拶</h2>
  <div class="wrapper flex">
    <div class="top_message_txt">
      当社は、昭和30年(1955年)に和歌山県岩出市（当時岩出町）にて創業し、昭和40年に株式会社になりました。
      以来、多くの建設工事に携わり社会のインフラ整備に貢献してまいりました。<br>
      おかげ様を持ちまして実績を積み重ね皆様方からの信用をいただいて今日まで事業を続けてこられたこと、深く感謝を申し上げます。<br><br>
      株式会社藤平組は、「高品質の工事完成」「職員の技術研鑽」「健康的な職員の融和作り」をモットーに心がけています。
      建設業は土木・建築工事、維持管理等全てにおいて高度化、
      IT化が進んでおり、品質及び環境におけるISO認証を取得し設計施工を営んでいる総合建設業として、当社の役割が重要だと確信しています。<br>
      高度な知識・技術・経験を蓄積した人財をもって設計から施工までの能力を有しており、
      安心して皆様の期待に応えられる会社でありますので、今後とも何卒よろしくお願い申し上げます。
    </div>
    <div class="top_message_profile">
      <img src="./image/company/top_message_profile.png">
      <div class="">株式会社 藤平組<br>代表取締役 <span>藤平 良光</span></div>
    </div>
  </div>
</section>


</main>
</div>

<?php include('footer.php'); ?>

</body>
</html>
