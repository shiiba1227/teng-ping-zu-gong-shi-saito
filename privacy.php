<!DOCTYPE html>
<?php
  $path = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
?>
<html lang="ja">
<head>
  <title>【公式】株式会社藤平組 | 地域に密着した和歌山県の総合建設会社です。</title>
  <meta content="株式会社藤平組は地域に密着した和歌山県の総合建設会社です。" name="description">
  <?php include('head.php'); ?>
  <link href="./css/privacy.css" rel="stylesheet" type="text/css">
</head>

<body id="home" class="home blog drawer drawer--right">
<div id="privacy">
<?php include('header.php'); ?>
<main>


<section id="title">
  <h1><span>PRIVACY&POLICY</span>個人情報保護方針</h1>
</section>

<div class="privacy_txt wrapper">
<p>
株式会社藤平組（以下当社）では、個人情報に関する法令およびその他の規範を遵守し、
お客様の大切な個人情報の保護に万全を尽くします。
</p>

<p><span>個人情報の収集について</span>
当社では、次のような場合に必要な範囲で個人情報を収集することがあります。
</p>

<p><span>当社へのお問い合わせ時</span>
個人情報の利用目的について
当社は、お客様から収集した個人情報を次の目的で利用いたします。
</p>
<p><span>お客様への連絡のため</span>
お客様からのお問い合せに対する回答のため
個人情報の第三者への提供について
当社では、お客様より取得した個人情報を第三者に開示または提供することはありません。
</p>

<p>ただし、次の場合は除きます。</p>

<p><span>ご本人の同意がある場合</span>
警察からの要請など、官公署からの要請の場合
法律の適用を受ける場合
個人情報の開示、訂正等について
当社は、お客様ご本人からの自己情報の開示、訂正、削除等のお求めがあった場合は、確実に応じます。
</p>
<p><span>個人情報保護に関するお問い合わせ先</span>
TEL. 0736-62-2041　FAX. 0736-62-8911
e-mail. fujihira@beach.ocn.ne.jp
株式会社藤平組
</p>

</div>


</main>
</div>

<?php include('footer.php'); ?>

</body>
</html>
