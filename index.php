<!DOCTYPE html>
<?php
  $path = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
?>
<html lang="ja">
<head>
  <title>【公式】株式会社藤平組 | 地域に密着した和歌山県の総合建設会社です。</title>
  <meta content="株式会社藤平組は地域に密着した和歌山県の総合建設会社です。" name="description">
  <?php include('head.php'); ?>
  <link href="./css/top.css" rel="stylesheet" type="text/css">
</head>

<body id="home" class="home blog drawer drawer--right">
<div id="top">
<?php include('header.php'); ?>

<main>
  <div id="loadingAnim" class="loadingAnim">
  <i class="loadingAnim_line"></i>
  </div>

<section id="mv">
  <div class="wrapper flex">
    <div class="mv_txt">
      <h1>長年培った経験と実績で<br>地域の笑顔を守りたい。</h1>
      <p>
        藤平組は「働く人ファースト」<br>
        お客様を大切にするとともに、<br class="sp">一緒に働く仲間を大切にしています。<br>
        採用募集中です。お気軽にご応募ください。
        <img class="sp" src="image/top/hikki.png">
      </p>
      <div class="btn flex pc">
        <a href="https://lin.ee/tc2pkAt" target="_blank"><div class="line_btn">LINEから応募</div></a>
        <a href="entry.php"><div class="form_btn">フォームで応募</div></a>
      </div>
        <div class="line_guide_btn pc">
        <a href="line.php">＞LINE応募の使い方はこちら</a>
      </div>
    </div>
    <div class="mv_img">
      <img src="image/top/pic_top_mv.png">
      <div class="btn flex sp">
        <a href="https://lin.ee/tc2pkAt" target="_blank"><div class="line_btn">LINEから応募</div></a>
        <a href="entry.php"><div class="form_btn">フォームで応募</div></a>
      </div>
      <div class="line_guide_btn sp">
      <a href="line.php">＞LINE応募の使い方はこちら</a>
    </div>
    </div>
  </div>
  <a class="scroll_bar" href="#">Scroll</a>
</section>

<section id="top-info">
  <div class="flex wrapper">
    <div class="top-info_title flex">
      <h3><span>INFORMATION</span><br>最新情報</h3>
    </div>
    <div class="top-info_txt">
      <ul>
        <li><a href="image/info/file.pdf" target="_blank">
          2020-08-05 国土交通省近畿地方整備局様より国土交通行政関係功労者における令和2年度優良工事等施工者(事務所長表彰)を頂きました。
        </a></li>
        <li><a href="image/info/IMG_4330.jpg" target="_blank">
          2020-07-31 国土交通省近畿地方整備局様より令和2年度工事成績優秀企業認定書を頂きました。
        </a></li>
        <li><a href="image/info/IMG_3681.jpg" target="_blank">
          2020-02-05 国土交通省近畿地方整備局様より感謝状を頂きました。
        </a></li>
        <li><a href="image/info/IMG_2949.jpg" target="_blank">
          2019-07-31 国土交通省近畿地方整備局様より令和元年度工事成績優秀企業認定書を頂きました。
        </a></li>
        <li><a href="image/info/IMG_3.jpg" target="_blank">
          2019-07-22 国土交通省近畿地方整備局様より国土交通行政関係功労者における令和元年度優良工事等施工者(局長表彰)を頂きました。
        </a></li>
      </ul>
    </div>
  </div>
</section>

<section id="catch">
  <div class="wrapper">
    <div class="catch_txt">
      <h3>「働く人ファースト」</h3>
      気持ちよく働ける環境づくりを考え<br class="sp">オフィスをリノベーションしたり、<br>
      本人の考えやタイミングを尊重して慎重に、<br>
      早すぎず遅すぎない<br class="sp">ステップアップしていただきます。<br>
      昭和30年創業以来、様々な<br class="sp">民間・公共工事の実績がある環境で<br>
      一緒に働いてみませんか？
      <a href="recruit.php"><div class="catch_btn form_btn">採用について詳しく見る</div></a>
    </div>
  </div>
</section>

<section id="top_case">
  <div>
    <h2><span>RECRUIT MESSAGE</span>採用メッセージ</h2>
    <div class="case_inner">
      <div class="flex wrapper">
      <a class="case_box scrollreveral" href="recruit.php#case1">
        <div>
          <span>CASE 01</span>
          仕事もプライベートもバランスよく。<br>
          ワークライフバランスを大切にできます！
        </div>
      </a>
      <a class="case_box scrollreveral2" href="recruit.php#case2">
        <div>
          <span>CASE 02</span>
          初めは誰でも未経験です。<br>
          専門学科に捉われないで大丈夫です♪
        </div>
      </a>
      <a class="case_box scrollreveral3" href="recruit.php#case3">
        <div>
          <span>CASE 03</span>
          福利厚生も充実した環境で<br>
          自分らしくいきいきと働けると思います。
        </div>
      </a>
      <a class="case_box scrollreveral4" href="recruit.php#case4">
        <div>
          <span>CASE 04</span>
          切れ者で優しく頼りになる監督さんがたくさんいます。精鋭集団の一員に是非！
        </div>
      </a>
      <div class="case_txt">ご応募をお待ちしております。<br class="sp">どうぞお気軽にお問い合わせください。</div>
        <div class="btn flex">
          <a href="https://lin.ee/tc2pkAt" target="_blank"><div class="line_btn">LINEから応募</div></a>
          <a href="entry.php"><div class="form_btn">フォームで応募</div></a>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="results">
  <div class="wrapper">
    <h2><span>RESULTS</span>施工実績</h2>
    <div class="results_txt">
      弊社では、公共工事・民間工事を問わず、各種の工事全般を承っております。<br>
      「地域密着・地元還元」を念頭に、地域の発展や豊かな暮らしづくりに貢献しています。<br>
      <a href="results.php">実績一覧はこちら ></a>
    </div>
    <div class="results_inner flex">
      <a class="results_box doboku scrollreveral" href="results.php">
        <div class="results_box_txt">
          <img src="image/top/doboku_icon.png">
          <p>土木実績</p>
        </div>
      </a>
      <a class="results_box archtect scrollreveral2" href="results.php">
        <div class="results_box_txt">
          <img src="image/top/architect_icon.png">
          <p>建築実績</p>
        </div>
      </a>
      <a class="results_box now scrollreveral3" href="results.php">
        <div class="results_box_txt">
          <img src="image/top/now_icon.png">
          <p>現在進行中</p>
        </div>
      </a>
    </div>

    <div class="results_pick flex scrollreveral4">
      <span>近年の災害対応</span>
      <div class="results_pick_img">
        <img src="image/top/results_pick.png">
      </div>
      <div class="results_pick_txt">
        <p>令和元年台風19号がもたらした長野県千曲川の河川堤防決壊においては、
        孤立した水害地域の早期復旧（国土交通省請負である災害対策車を現地派遣し
        排水ポンプの設置操作)に協力させていただきました。</p>
      </div>
    </div>
  </div>
</section>

<section id="top_contents">
  <div class="wrapper flex">
    <a class="contents_box" href="company.php">
        <img src="image/top/company.png">
        <p>会社概要</p>
    </a>
    <a class="contents_box" href="support.php">
        <img src="image/top/support.png">
        <p>サポート情報</p>
    </a>
    <a class="contents_box" href="line.php">
        <img src="image/top/lineguide.png">
        <p>LINE応募使い方ガイド</p>
    </a>
    <a class="contents_box" href="recruit.php">
        <img src="image/top/joblist.png">
        <p>求人一覧</p>
    </a>
  </div>
</section>

</main>
</div>

<?php include('footer.php'); ?>

<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
<script type="text/javascript" charset="utf-8">
var $delayTime = 1200;
$(window).on('load', function(){
    var $loadingAnim = $('#loadingAnim'),
        $body = $('body');
    setTimeout( function(){
        $body.addClass('loaded');
        $loadingAnim.find('.loadingAnim_line').on('transitionend', function( e ){
            $(this).parent().remove();
        });
    }, $delayTime );
});
</script>
</body>
</html>
