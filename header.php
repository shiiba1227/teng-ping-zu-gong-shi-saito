<header id="header">
  <div class="wrapper">
    <header class="head">
      <div class="logo"><a href="./"><img src="./image/common/logo.png" alt="藤平組ロゴ"></a></div>
    </header>
    <div id="navpc">
        <ul class="glnav">
          <li class="glnav-item"><a href="./">TOP</a></li>
          <li class="glnav-item"><a href="./company.php">企業情報</a></li>
          <li class="glnav-item"><a href="./results.php">実績一覧</a></li>
          <li class="glnav-item"><a href="./joblist.php">求人一覧</a></li>
          <li class="glnav-item"><a href="./support.php">サポート情報</a></li>
          <li class="glnav-item"><a href="./line.php">LINE応募ガイド</a></li>
          <li class="glnav-item glnav-recruit_btn"><a href="./recruit.php">採用情報</a></li>
        </ul>
    </div>
    <div id="navsp">
      <div class="memubtn" id="memubtn"></div>
      <nav class="nav" id="nav">
        <div class="nav-inner">
          <ul class="glnav">
            <li class="glnav-item"><a href="./">TOP</a></li>
            <li class="glnav-item"><a href="./company.php">企業情報</a></li>
            <li class="glnav-item"><a href="./results.php">実績一覧</a></li>
            <li class="glnav-item"><a href="./joblist.php">求人一覧</a></li>
            <li class="glnav-item"><a href="./support.php">サポート情報</a></li>
            <li class="glnav-item"><a href="./line.php">LINE応募ガイド</a></li>
            <li class="glnav-item glnav-recruit_btn"><a href="./recruit.php">採用情報</a></li>
          </ul>
        </div>
      </nav>
    </div>
  </div>
</header>

<article class="sidemenu">
  <a href="https://lin.ee/tc2pkAt" target="_blank"><img src="./image/common/btn_sidemenu_line.png"></a>
  <a href="entry.php"><img src="./image/common/btn_sidemenu_recruit.png"></a>
</article>
