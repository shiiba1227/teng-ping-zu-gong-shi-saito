<!DOCTYPE html>
<?php
  $path = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
?>
<html lang="ja">
<head>
  <title>【公式】株式会社藤平組 | 地域に密着した和歌山県の総合建設会社です。</title>
  <meta content="株式会社藤平組は地域に密着した和歌山県の総合建設会社です。" name="description">
  <?php include('head.php'); ?>
  <link href="./css/thanks.css" rel="stylesheet" type="text/css">
</head>

<body id="home" class="home blog drawer drawer--right">
<div id="thanks">
<?php include('header.php'); ?>
<main>


<section id="title">
  <h1><span>THANKS</span>応募が完了しました。</h1>
</section>

<div class="thanks">
  <p><span>ご記入頂いた情報は無事送信されました。</span><br>
  <br>
  ご応募頂きましたら、<br>担当よりお電話にてご連絡させて頂きます。<br>
  <br>
  この度は、ご応募誠にありがとうございました。</p>
</div>

</div>
</main>
</div>
<?php include('footer.php'); ?>

</body>
</html>
