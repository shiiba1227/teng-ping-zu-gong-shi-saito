<!DOCTYPE html>
<?php
  $path = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
?>
<html lang="ja">
<head>
  <title>【公式】株式会社藤平組 | 地域に密着した和歌山県の総合建設会社です。</title>
  <meta content="株式会社藤平組は地域に密着した和歌山県の総合建設会社です。" name="description">
  <?php include('head.php'); ?>
  <link href="./css/entry.css" rel="stylesheet" type="text/css">
</head>

<body id="home" class="home blog drawer drawer--right">
<div id="entry">
<?php include('header.php'); ?>
<main>


<section id="title">
  <h1><span>ENTRY</span>採用応募フォーム</h1>
</section>

<form id="mailform" method="post" action="./mailform/send.cgi" onsubmit="return sendmail(this);">
<div class="item">
  <label class="label" for="お名前">お名前</label>
  <input class="inputs" type="text" name="お名前" placeholder="例)鈴木 一朗" required="required">
</div>

<div class="item">
  <label class="label" for="電話番号">電話番号</label>
  <input class="inputs" type="number" name="電話番号" placeholder="例)0736770006" required="required">
  <p class="supplement"> ※ハイフン(-)なし、半角数字で入力ください。</p>
</div>

<div class="item">
  <p class="label">運転免許</p>
  <div class="inputs">
  <input class="inputs" id="AT" type="radio" name="運転免許" value="AT免許" required="required"><label class="radio" for="AT免許">AT免許</label><br>
  <input class="inputs" id="MT" type="radio" name="運転免許" value="MT免許"><label class="radio" for="MT免許">MT免許</label><br>
  <input class="inputs" id="nodrive" type="radio" name="運転免許" value="なし"><label class="radio" for="なし">なし</label><br>
</div>
</div>

<div class="item">
  <label class="label" for="生年月日">生年月日</label>
  <input class="inputs" type="text" name="生年月日" placeholder="例)1984年10月2日" required="required">
</div>

<div class="item">
  <p class="label">経験</p>
<div class="inputs">
  <input class="inputs" id="doboku" type="radio" name="経験" value="土木工事"><label class="radio" for="土木工事">土木工事</label><br>
  <input class="inputs" id="kenchiku" type="radio" name="経験" value="建築工事"><label class="radio" for="建築工事">建築工事</label><br>
  <input class="inputs" id="sokuryo" type="radio" name="経験" value="測量業務"><label class="radio" for="測量業務">測量業務</label><br>
  <input class="inputs" id="other" type="radio" name="経験" value="その他(図面作成・積算・見積り etc)"><label class="radio" for="その他(図面作成・積算・見積り etc)">その他(図面作成・積算・見積り etc)</label><br>
</div>
</div>

<div class="item">
  <label class="label" for="メールアドレス">メールアドレス</label>
  <input class="inputs" type="email" name="email" placeholder="fujihira@beach.ocn.ne.jp">
</div>

<div class="item">
  <label class="label" for="住所">住所</label>
  <input class="inputs" type="text" name="住所" placeholder="例)和歌山県岩出市中黒164-4">
</div>

<div class="item">
  <label class="label" for="お問い合わせ">お問い合わせ<br class="sp">内容</label>
  <textarea class="inputs faq" name="お問い合わせ" placeholder="例)面接は私服でも大丈夫ですか？"></textarea>
</div>

<p style="text-align:center;font-size: 1.3rem;">ご応募の際は弊社の個人情報保護方針に同意していただいたとします。</p>

<div class="btn-area">
  <input class="inputs hover1" type="submit" value="送信する">
</div>

</form>

</div>
</main>
</div>
<?php include('footer.php'); ?>
</body>
</html>
