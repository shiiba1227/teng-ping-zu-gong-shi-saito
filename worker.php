<!DOCTYPE html>
<?php
  $path = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
?>
<html lang="ja">
<head>
  <title>【公式】株式会社藤平組 | 地域に密着した和歌山県の総合建設会社です。</title>
  <meta content="株式会社藤平組は地域に密着した和歌山県の総合建設会社です。" name="description">
  <?php include('head.php'); ?>
  <link href="./css/joblist.css" rel="stylesheet" type="text/css">
</head>

<body id="home" class="home blog drawer drawer--right">
<div id="job_detail">
<?php include('header.php'); ?>
<main>


<section id="title">
  <h1><span>JOB DETAIL</span>現場スタッフ</h1>
</section>

<section class="job_detail wrapper">
  <h3>
  <span>株式会社 藤平組</span>
  ［和歌山県岩出市］働く人ファーストを掲げ、地元地域に高度なインフラ技術で貢献します！
  現場スタッフ（岩出市中黒）。幅広い知識と経験を蓄積していただきます。
  </h3>
  <div class="job_detail_box flex">
    <div class="job_detail_img">
      <img src="./image/joblist/worker.png">
    </div>
    <div class="job_detail_txt">
      <h4>キャリアアップの例</h4>
      <span>STEP1</span><br>
      土木・建築・河川維持における作業に携わっていただきます。<br>
      <span>STEP2</span><br>
      技術職におけるスキルアップを追求していただきます。<br>
      また、管理職（施工管理補助）として職種を変更し、キャリアアップをしていただくことも可能です。<br>
      適性を考慮し、最適なキャリアアップを会社が親身になってご相談させていただき、将来を決定させていただきます。<br>
      <br>
    </div>

    <table>
      <tr><th>雇用形態</th><td>正社員</td></tr>
      <tr><th>派遣・請負等</th><td>派遣・請負ではない</td></tr>
      <tr><th>仕事の内容</th><td>○土木、建築、河川維持工事に伴う作業及び各種管理補助業務を行って頂きます。☆業務の中で様々な資格取得を会社が支援します。（建設機械免許等のさまざまな講習・特別教育・安全衛生教育全　　般、準中型・中型・大型自動車等の資格取得支援、又安全に関わ　るスキル講座等は会社でも実施します）◎意欲があれば、現場監督も目指せます。・施工管理資格の取得も会社が支援します。◎やる気と意欲を大切にします。　【建設】</td></tr>
      <tr><th>雇用期間</th><td>雇用期間の定めなし</td></tr>
      <tr><th>必要な経験等</th><td>不問</td></tr>
      <tr><th>年齢</th><td>制限あり</td></tr>
      <tr><th>勤務地</th><td>和歌山県岩出市中黒１６４番地の４</td></tr>
      <tr><th>マイカー通勤</th><td>可</td></tr>
      <tr><th>転勤</th><td>なし</td></tr>
      <tr><th>賃金</th><td>月給 249,700円 ～ 317,800円</td></tr>
      <tr><th>賞与</th><td>賞与制度の有無あり</td></tr>
      <tr><th>通勤手当</th><td>実費支給（上限あり）</td></tr>
      <tr><th>就業時間</th><td>休憩時間:90分/年間休日:92日</td></tr>
      <tr><th>休日</th><td>日曜日，祝日，その他/週休二日制</td></tr>
      <tr><th>加入保険</th><td>雇用保険，労災保険，健康保険，厚生年金</td></tr>
      <tr><th>定年制</th><td>あり</td></tr>
      <tr><th>再雇用制度</th><td>あり</td></tr>
      <tr><th>入居可能住宅</th><td>なし</td></tr>
      <tr><th>利用可能な託児所</th><td>なし</td></tr>
      <tr><th>育児休業取得実績</th><td>なし</td></tr>
      <tr><th>選考方法</th><td>面接（予定1回）</td></tr>
      <tr><th>選考日時</th><td>随時</td></tr>
      <tr><th>応募書類等</th><td>履歴書（写真貼付）</td></tr>
      <tr><th>求人に関する特記事項</th><td>＊基本的に休日出勤はなし（但し、災害時は除く）＊時間外月平均１０時間は、毎日３０分程度（片付け、翌日準備等）＊残業代を含めると２７万～３５万程度の支給になります＊福利厚生も充実。作業服、ヘルメット、長靴、空調服、　飲料水、清涼シート、フルハーネスも随時無償支給＊建設業退職金共済に加入＊建設業福祉共済団に加入←労働災害における副次的補償も充実・マイカー通勤について：無料駐車場あり＊阪南地方の方も現在２名在籍中＊有給休暇消化率７２％＊週休二日対象工事に対する閉所日※は、給与補填いたします。　※会社は出勤日である。</td></tr>
    </table>
    <a class="recruit_bnr" href="./recruit.php">
      <img src="image/joblist/recruit.png">
    </a>
  </div>
</section>

<div class="job_detail_cv">
<div class="btn flex">
  <a href=""><div class="line_btn">LINE応募</div></a>
  <a href="entry.php"><div class="form_btn">応募する</div></a>
</div>
<a class="line_bnr" href="./line.php">
  <img src="image/joblist/line.png">
</a>
</div>

</main>
</div>

<?php include('footer.php'); ?>

</body>
</html>
