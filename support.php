<!DOCTYPE html>
<?php
  $path = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
?>
<html lang="ja">
<head>
  <title>【公式】株式会社藤平組 | 地域に密着した和歌山県の総合建設会社です。</title>
  <meta content="株式会社藤平組は地域に密着した和歌山県の総合建設会社です。" name="description">
  <?php include('head.php'); ?>
  <link href="./css/support.css" rel="stylesheet" type="text/css">
</head>

<body id="home" class="home blog drawer drawer--right">
<div id="support">
<?php include('header.php'); ?>
<main>


<section id="title">
  <h1><span>Q&A</span>サポート情報</h1>
</section>

<article class="support">
<div class="wrapper">
  <dl>
    <dt>見積には料金が発生しますか？</dt>
    <dd>見積は無料です。どんな些細なことでもお気軽にお問い合わせ下さい。</dd>
  </dl>
</div>
</article>

</div>


</main>
</div>

<?php include('footer.php'); ?>

</body>
</html>
