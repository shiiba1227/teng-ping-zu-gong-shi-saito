<!DOCTYPE html>
<?php
  $path = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
?>
<html lang="ja">
<head>
  <title>【公式】株式会社藤平組 | 地域に密着した和歌山県の総合建設会社です。</title>
  <meta content="株式会社藤平組は地域に密着した和歌山県の総合建設会社です。" name="description">
  <?php include('head.php'); ?>
  <link href="./css/joblist.css" rel="stylesheet" type="text/css">
</head>

<body id="home" class="home blog drawer drawer--right">
<div id="job_detail">
<?php include('header.php'); ?>
<main>


<section id="title">
  <h1><span>JOB DETAIL</span>施工管理</h1>
</section>

<section class="job_detail wrapper">
  <h3>
  <span>株式会社 藤平組</span>
  ［和歌山県岩出市］働く人ファーストを掲げ、地元地域に高度なインフラ技術で貢献します！
  施工管理（岩出市中黒）。幅広い知識と経験を活かし活躍していただきます。
  </h3>
  <div class="job_detail_box flex">
    <div class="job_detail_img">
      <img src="./image/joblist/sekokan.png">
    </div>
    <div class="job_detail_txt">
      <h4>施工管理（現場監督）業務</h4>
      即戦力として、<br>
      官公庁及び民間工事に携わっていただきます。
    </div>

    <table>
      <tr><th>職種</th><td>施工管理</td></tr>
      <tr><th>仕事内容</th><td>公共工事を主とする施工 と管理<br>
      ※1級土木又は建築施工管理技士の資格を有する方 (2級施工管理技士の資格を有する方については、 相談に応じます）</td></tr>
      <tr><th>雇用形態</th><td>正社員</td></tr>
      <tr><th>派遣・請負等</th><td>派遣・請負ではない</td></tr>
      <tr><th>雇用期間</th><td>定め無し</td></tr>
      <tr><th>就業場所</th><td>〒649-6248<br>
        和歌山県岩出市中黒164番地の4<br>
        JR紀伊駅から車5分<br>
        ※屋内の受動喫煙対策 あり（禁煙）<br>
        ※屋内に喫煙所あり</td></tr>
      <tr><th>マイカー通勤</th><td>可（駐車場あり）</td></tr>
      <tr><th>年齢</th><td>64歳以下</td></tr>
      <tr><th>学歴</th><td>高卒以上</td></tr>
      <tr><th>必要な 経験・知識・技能等</th><td>土木又は建築工事経験のいずれか</td></tr>
      <tr><th>必要な 免許・資格</th><td>【いずれかの免許・資格所持で可】<br>
      1級土木施工管理技士、1級建築施工管理技士<br>
      ※2級施工管理技士は応相談<br>
      【必須】<br>
      普通自動車迎転免許（AT限定不可）</td></tr>
      <tr><th>試用期間</th><td>１ヶ月</td></tr>
      <tr><th>賃金</th><td>月給280,000円〜400,000円</td></tr>
      <tr><th>賞与</th><td>賞与制度の有無あり</td></tr>
      <tr><th>通勤手当</th><td>実費支給（上限あり）</td></tr>
      <tr><th>就業時間</th><td>08時00分～17時00分</td></tr>
      <tr><th>休日</th><td>休憩時間:90分/年間休日:92日<br>
      ※当社カレンダーによる。盆、年末年始</td></tr>
      <tr><th>加入保険</th><td>雇用保険，労災保険，健康保険，厚生年金</td></tr>
      <tr><th>応募書類等</th><td>履歴書（写真貼付）</td></tr>
      <tr><th>求人に関する特記事項</th><td>＊平均有給取得日数：1 3. 8日(R 1年度）<br>
      ＊マイカ ー通勤ついて：無料駐車場あり<br>
      ＊社有車貸与<br>
      ＊社内のバックアップ体制が充実しており 業務の負担軽減に努めています。<br>
      ＊建設業退職金共済に加入←中小企業退職金共済とどちらでも可。<br>
      ＊建設業福祉共済団に加入←労働災害における副次的補償も充実しています。<br>
      福利厚生も充実。作業服、 ヘ ルメット 、長靴、 空調服、 飲料水、 渚涼シート、フルハーネスも随時無償支給<br>
      阪南地方の方も現在2名在籍中</td></tr>
    </table>
    <a class="recruit_bnr" href="./recruit.php">
      <img src="image/joblist/recruit.png">
    </a>
  </div>
</section>

<div class="job_detail_cv">
<div class="btn flex">
  <a href=""><div class="line_btn">LINE応募</div></a>
  <a href="entry.php"><div class="form_btn">応募する</div></a>
</div>
<a class="line_bnr" href="./line.php">
  <img src="image/joblist/line.png">
</a>
</div>

</main>
</div>

<?php include('footer.php'); ?>

</body>
</html>
