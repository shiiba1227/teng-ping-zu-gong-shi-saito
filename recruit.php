<!DOCTYPE html>
<?php
  $path = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
?>
<html lang="ja">
<head>
  <title>【公式】株式会社藤平組 | 地域に密着した和歌山県の総合建設会社です。</title>
  <meta content="株式会社藤平組は地域に密着した和歌山県の総合建設会社です。" name="description">
  <?php include('head.php'); ?>
  <link href="./css/recruit.css" rel="stylesheet" type="text/css">
</head>

<body id="home" class="home blog drawer drawer--right">
<div id="recruit">
<?php include('header.php'); ?>
<main>


<section id="title">
  <h1><span>RECRUIT</span>採用情報</h1>
  <p>長年培った経験と実績で地域の笑顔を守りたい。</p>
</section>

<h2><span>MESSAGE</span>採用メッセージ</h2>
<p class="message_txt">働く人ファーストを掲げています。<br>働きやすい環境で<br class="sp">自分らしくステップアップ<br class="sp">していってください。</p>

<section class="job_detail_box flex wrapper">
  <div class="job_detail_img">
    <img src="./image/top/joblist.png">
  </div>
  <div class="job_detail_txt">
    <h4>キャリアアップの例</h4>
    <span>STEP1</span><br>
    施工管理(現場監督)業務<br>
    最初はさまざなな現場における補助業務に携わっていただく(もちろん本人にﾋｱﾘﾝｸﾞし適正を考慮)ことで、 幅広い知識と経験を蓄積していただきます。<br>
    <span>STEP2</span><br>
    官公庁及び民間の新築工事、改修工事などの現場管理を上司の指導の基、業務していただきます。 また、この間に次のステップへの礎として、国家資格(1、2級建築施工管理技士・1、2級土木施工管理技士)を取得していだきます。<br>
    <span>STEP3</span><br>
    官公庁及び民間の新築工事、改修工事などの現場管理を自身の裁量の基、業務していただきます。 <br>
    あくまで目安であり、<div class="red">遅すぎず早すぎず本人の考えや意見タイミングを踏まえて慎重にステップアップしていただきます。</div><br>
    <a href="joblist.php"><div class="form_btn recruit_btn">求人情報一覧</div></a>
  </div>
</section>

<section class="recruit_point">
  <h2><span>6POINT</span>6つのポイント</h2>
  <div class="point_inner flex wrapper">
    <div class="point_box">
      <img src="./image/recruit/point1.png">
      <p>有休消化率83％<span>※2019年度実績</span></p>
    </div>
    <div class="point_box">
      <img src="./image/recruit/point2.png">
      <p>未経験歓迎</span></p>
    </div>
    <div class="point_box">
      <img src="./image/recruit/point3.png">
      <p>専門学科に<br>とらわれなくてOK</span></p>
    </div>
    <div class="point_box">
      <img src="./image/recruit/point4.png">
      <p>福利厚生充実！</span></p>
    </div>
    <div class="point_box">
      <img src="./image/recruit/point5.png">
      <p>業務グッズ随時支給<span>※飲料水・空調服・清涼シートetc</span></p>
    </div>
    <div class="point_box">
      <img src="./image/recruit/point6.png">
      <p>精鋭の先輩がレクチャー<span>※国土交通省より表彰歴あり</span></p>
    </div>
  </div>
</section>

<section class="recruit_message">
  <h2><span>RECRUIT MESSAGE</span>採用メッセージ</h2>
  <div class="wrapper">
  <div class="case_inner">
    <h4 id="case1"><span>CASE 01</span>仕事もプライベートもバランスよく。ワークライフバランスを大切にできます！</h4>
    <div class="case_txt">
      社員の年間平均有休取得日数は16.3日(2019年度)であり、
      一人当たりの平均有休消化率は83％となっています。
      プライベートの時間も大切にしながら、ぜひキャリアアップしてほしいと期待しています。
    </div>
  </div>
  <div class="case_inner">
    <h4 id="case2"><span>CASE 02</span>初めは誰でも未経験です。専門学科に捉われないで大丈夫です♪</h4>
    <div class="case_txt">
      不安もいっぱいあるでしょうが、優しい先輩方がしっかり教えてくれますので、安心してください。
      一歩一歩しっかりと着実に歩んでいってください。他業種・他学部・他学科でも、やる気・意欲があれば、
      一人前になれる環境が当社にはありますので、土木・建築学科出身に関係なく是非ご応募下さい。
      (電気や機械等、また文系の方達も大いに監督としてご活躍されています)
    </div>
  </div>
  <div class="case_inner">
    <h4 id="case3"><span>CASE 03</span>福利厚生も充実した環境で自分らしくいきいきと働けます。</h4>
    <div class="case_txt">
      仲間との親睦を深めるイベント(飲み会や社内コンペ)はもちろん、
      普段の業務におけるグッズ【会社で用意してくれたら助かる物（飲料水・空調服・清涼シートetc）】等も随時支給させていただきます。
      あなたが働き易い環境を常に追求します。
    </div>
  </div>
  <div class="case_inner">
    <h4 id="case4"><span>CASE 04</span>切れ者で優しく頼りになる監督さんがたくさんいます。精鋭集団の一員に是非！</h4>
    <div class="case_txt">
      切れ者で優しく頼りになる監督さんがたくさんいます。
      そんな監督さん達のおかげで、近年では、国土交通省近畿地方整備局様より優良工事施工者表彰(局長表彰)及び
      工事成績優秀企業認定という2つの表彰をいただきました。
      そうした表彰に導いてくれた監督さんの背中を追いかけ、成長できる環境が当社にはあります。是非当社を自己研鑽の場にして下さい。
    </div>
  </div>
</div>
</section>

<div class="job_detail_cv">
<div class="btn flex">
  <a href="https://lin.ee/tc2pkAt" target="_blank"><div class="line_btn">LINE応募</div></a>
  <a href="entry.php"><div class="form_btn">応募する</div></a>
</div>
<a class="line_bnr" href="./line.php">
  <img src="image/joblist/line.png">
</a>
</div>

</div>
</main>
</div>

<?php include('footer.php'); ?>

</body>
</html>
